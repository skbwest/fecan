#-------------------------------------------------
#
# Project created by QtCreator 2013-10-30T10:56:05
#
#-------------------------------------------------

QT       += core gui
QT += gui declarative

TARGET = my_gui
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    rs232.c

HEADERS  += mainwindow.h \
    rs232.h

FORMS    += mainwindow.ui
